import uuid

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_spot_instance(hub, ctx, aws_ec2_subnet):
    """
    Request instance and ensure spot instance request ID matches describe.
    """
    # Request instance
    instance_temp_name = "idem-test-instance-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": instance_temp_name}]
    ret = await hub.states.aws.ec2.spot_instance.present(
        ctx,
        name=instance_temp_name,
        launch_specification={"SubnetId": aws_ec2_subnet.get("SubnetId")},
        tags=tags,
    )
    assert ret["result"], True
    resource_id = ret["comment"]

    # Describe instance
    describe_ret = await hub.states.aws.ec2.spot_instance.describe(ctx)
    assert (
        resource_id
        in describe_ret["None"]["aws.ec2.spot_instance.present"][2][
            "spot_instance_request_id"
        ]
    )

    # Delete instance
    ret = await hub.states.aws.ec2.instance.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
